'use strict'

exports.getIndex = (req, res) => {
    if (req.session.isLoggedIn) {
        res.render('index', {
            userName: req.session.userName,
            password: req.session.password
        });
    } else {
        res.redirect('/login');
    }
};